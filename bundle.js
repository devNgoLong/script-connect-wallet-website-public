/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./script.ts":
/*!*******************!*\
  !*** ./script.ts ***!
  \*******************/
/***/ ((__unused_webpack_module, exports) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nlet isConnected = false;\n// Biến để lưu thời điểm lần click cuối cùng\nlet lastClickTime = 0;\n// Hằng số thời gian giữa hai lần click\nconst DOUBLE_CLICK_INTERVAL = 500; // milliseconds\n// Kiểm tra nếu không phải là mạng Base Mainnet thì điều hướng chọn mạng\nfunction redirectToCorrectNetwork() {\n    const desiredNetwork = {\n        networkName: \"Base Mainnet\",\n        rpcEndpoint: \"https://mainnet.base.org\",\n        chainId: 8453\n    };\n    if (window.ethereum) {\n        window.ethereum.request({ method: 'eth_chainId' })\n            .then((chainId) => {\n            if (parseInt(chainId, 16) !== desiredNetwork.chainId) {\n                if (confirm(`Ứng dụng cần chuyển sang mạng ${desiredNetwork.networkName}. Bạn có muốn chuyển không?`)) {\n                    window.ethereum.request({\n                        method: 'wallet_switchEthereumChain',\n                        params: [{ chainId: `0x${desiredNetwork.chainId.toString(16)}` }]\n                    }).catch((error) => {\n                        if (error.code === 4902) {\n                            alert(`Vui lòng thêm mạng ${desiredNetwork.networkName} vào MetaMask và thử lại.`);\n                        }\n                        else {\n                            console.error(error);\n                        }\n                    });\n                }\n            }\n        })\n            .catch((error) => {\n            console.error(\"Lỗi khi lấy thông tin mạng:\", error);\n        });\n    }\n    else {\n        alert(\"Vui lòng cài đặt MetaMask để tiếp tục.\");\n    }\n}\ndocument.addEventListener(\"DOMContentLoaded\", () => {\n    redirectToCorrectNetwork();\n    const connectButton = document.getElementById(\"kxnsm22n\");\n    if (connectButton) {\n        const connectWallet = () => {\n            const currentTime = Date.now();\n            // Kiểm tra nếu có thời gian giữa hai lần click nhỏ hơn DOUBLE_CLICK_INTERVAL\n            if (currentTime - lastClickTime < DOUBLE_CLICK_INTERVAL) {\n                // Nếu đã kết nối, thực hiện ngắt kết nối\n                if (isConnected) {\n                    isConnected = false;\n                    localStorage.removeItem('ethereumAddress');\n                    connectButton.textContent = \"Connect Wallet\";\n                    console.log(\"Đã ngắt kết nối\");\n                }\n                // Trả về để không thực hiện kết nối mới\n                return;\n            }\n            lastClickTime = currentTime;\n            if (window.ethereum) {\n                window.ethereum.request({ method: 'eth_requestAccounts' })\n                    .then((accounts) => {\n                    connectButton.textContent = `${accounts[0].slice(0, 6)}...${accounts[0].slice(-4)}`;\n                    console.log(\"Kết nối thành công với địa chỉ:\", accounts[0]);\n                    isConnected = true;\n                    localStorage.setItem('ethereumAddress', accounts[0]); // Lưu địa chỉ vào localStorage\n                })\n                    .catch((error) => {\n                    console.error(\"Lỗi kết nối:\", error);\n                });\n            }\n            else {\n                alert(\"Vui lòng cài đặt MetaMask để tiếp tục.\");\n            }\n        };\n        connectButton.addEventListener(\"click\", connectWallet);\n        const savedAddress = localStorage.getItem('ethereumAddress');\n        if (savedAddress) {\n            connectButton.textContent = `${savedAddress.slice(0, 6)}...${savedAddress.slice(-4)}`;\n            isConnected = true;\n        }\n    }\n    else {\n        console.error(\"Không tìm thấy connectButton trong DOM.\");\n    }\n});\n\n\n//# sourceURL=webpack://cdn-connect-wallet/./script.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./script.ts"](0, __webpack_exports__);
/******/ 	
/******/ })()
;